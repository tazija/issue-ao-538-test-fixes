package net.java.ao.schema.helper;

public interface Index
{
    String getTableName();

    String getFieldName();

    String getIndexName();
}
